from sympy import nsolve
from sympy import Symbol
import math

x,y = Symbol('x'),Symbol('y')

print nsolve([2*(x+y)**2 + (x-y)**2 - 8, 5*x**2+(y-3)**2 - 9],[x,y],[1,1])
